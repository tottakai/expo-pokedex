import * as React from 'react'

import { Text, TextProps } from './Themed'

export const MonoText = (props: TextProps): JSX.Element | null => <Text {...props} style={[props.style, { fontFamily: 'space-mono' }]} />
