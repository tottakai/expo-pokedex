import { fold } from 'fp-ts/lib/Either'
import { pipe } from 'fp-ts/lib/function'
import React, { useState } from 'react'
import { useEffect } from 'react'
import { Text, StyleSheet } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Pokemon } from '../../model/model'
import { getPokemonList } from '../../network/pokeapi'
import { PokemonList } from './PokemonList'

export const HomeScreen = (): JSX.Element => {
  const [offset, setOffset] = useState(0)
  const [pokemons, setPokemons] = useState<Pokemon[]>([])

  useEffect(() => {
    void getPokemonList(offset).then((res) => 
      pipe(res, fold((error) => {
        console.warn('Get pokemon list error:', error)
      }, (pokemonList) => setPokemons(p => [...p, ...pokemonList.results])))
    )
  }, [offset])

  return (
    <SafeAreaView style={styles.container} edges={['right', 'left']}>
      {pokemons.length === 0 ? 
        <Text>Loading...</Text> :
        <PokemonList 
          pokemons={pokemons}
          loadMore={() => setOffset(offset+10)}
        />
      }
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
})
