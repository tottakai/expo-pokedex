import React, { ComponentType } from 'react'
import { useNavigation } from '@react-navigation/native'
import { fold } from 'fp-ts/lib/Either'
import { pipe } from 'fp-ts/lib/function'
import { useState } from 'react'
import { useEffect } from 'react'
import { View, Text, StyleSheet, Image, TouchableNativeFeedback, Platform, TouchableHighlight, TouchableHighlightProps, TouchableNativeFeedbackProps } from 'react-native'
import { Pokemon, PokemonDetails, Type } from '../../model/model'
import { typeColor } from '../../model/pokemonTypeColor'
import { getPokemonDetails } from '../../network/pokeapi'

export const PokemonTile = ({item}: {item: Pokemon}): JSX.Element => {
  const [details, setDetails] = useState<PokemonDetails | null>(null)

  useEffect(() => {
    const abortController = new AbortController()

    void getPokemonDetails(item.url, abortController.signal).then((res) => 
      pipe(res, fold((err) => {
        console.warn('Details fetch error', err)
      }, (details) => setDetails(details)))
    )
    return () => abortController.abort()
  }, [item])

  return (
    details === null ?
      (<View style={styles.tile}>
        <Text>loading...</Text>
      </View>) :
      (<LoadedTile 
        details={details}
      />)
  )
}

const LoadedTile = ({details}: {details: PokemonDetails}) => {
  const navigation = useNavigation()

  const Touchable: ComponentType<TouchableHighlightProps | TouchableNativeFeedbackProps> = 
    Platform.OS === 'ios' ? TouchableHighlight :  TouchableNativeFeedback
  return (
    <Touchable style={styles.touchable}
      onPress={() => navigation.navigate('Details', { details, title: details.name })}
    >
      <View style={styles.tile}>
        <View style={styles.typeColors}>
          {details.types.map((type: Type, idx: number) => (
            <View
              key={`${idx}`}
              style={{backgroundColor: typeColor(type), width: `${100/details.types.length}%`, height: '100%'}} 
            />
          ))}
        </View>
        <View style={styles.pokemonImageContainer}>
          <Image
            style={styles.image}
            source={{uri: details.sprites.front_default}}
          />
        </View>
      </View>
    </Touchable>
  )
}

const styles = StyleSheet.create({
  touchable: { 
    flex:1,
  },
  typeColors: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
  tile: {
    flex: 0.5,
    flexDirection: 'column',
    aspectRatio: 1,
  },
  pokemonImageContainer: {
    flex: 1,
    justifyContent:'center', 
    alignItems: 'center',
  },
  image: {
    width: '70%',
    height: '70%',
  },
})