import React from 'react'
import { FlatList } from 'react-native'
import { Pokemon } from '../../model/model'
import { PokemonTile } from './PokemonTile'

interface Props {
  pokemons: Pokemon[]
  loadMore: () => void
}

export const PokemonList = ({ pokemons, loadMore }: Props): JSX.Element => (
  <FlatList
    data={pokemons}
    renderItem={(item) => (
      <PokemonTile
        item={item.item}
      />
    )}
    numColumns={2}
    keyExtractor={(item, index) => `K_${index}`}
    onEndReachedThreshold={0}
    onEndReached={() => loadMore()}
  />
)
