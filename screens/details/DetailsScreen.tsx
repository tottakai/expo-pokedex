import React, { useEffect } from 'react'
import { StyleSheet, ScrollView } from 'react-native'
import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { fold } from 'fp-ts/lib/Either'
import { pipe } from 'fp-ts/lib/function'
import { useState } from 'react'
import { PokemonSpecies } from '../../model/model'
import { getPokemonSpecies } from '../../network/pokeapi'
import { RootStackParamList } from '../../types'
import { Profile } from './Profile'
import { Summary } from './Summary'
import { Types } from './Types'

type DetailsScreenRouteProp = RouteProp<
  RootStackParamList,
  'Details'
>

type DetailsScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Details'
>

interface Props {
  route: DetailsScreenRouteProp
  navigation: DetailsScreenNavigationProp
}

export const DetailsScreen = ({ route, navigation }: Props): JSX.Element => {
  const { details } = route.params
  const [species, setSpecies] = useState<PokemonSpecies | null>(null)

  useEffect(() => {
    const abortController = new AbortController()

    void getPokemonSpecies(details.species.url, abortController.signal).then((res) => 
      pipe(res, fold((err) => {
        console.warn('Details fetch error', err)
      }, (species) => setSpecies(species)))
    )
    return () => abortController.abort()
  }, [navigation, details])

  return (
    <ScrollView style={styles.screen}>
      <Types types={details.types} />
      <Summary details={details} />
      <Profile details={details} species={species} />
    </ScrollView>
  )
}


const styles = StyleSheet.create({
  screen: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  }
})