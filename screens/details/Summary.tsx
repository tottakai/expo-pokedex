import React, { useState } from 'react'
import { useEffect } from 'react'
import { View, Text, StyleSheet, Animated } from 'react-native'
import { PokemonDetails } from '../../model/model'

const PokemonImage = ({imageUrl}:{imageUrl: string}): JSX.Element => {
  const [opacity] = useState(new Animated.Value(0))
  
  useEffect(() => {
    Animated.timing(opacity, {
      toValue: 1,
      duration: 700,
      useNativeDriver: true,
    }).start()
  }, [opacity])

  return (
    <Animated.View
      style={[styles.pokemonImageContainer, {opacity}]}
    >
      <Animated.Image
        style={[styles.image]}
        source={{uri: imageUrl}}
        resizeMode={'contain'}
      />
    </Animated.View>
  )
}

export const Summary = ({ details }: { details: PokemonDetails }): JSX.Element => {
  const maxStat = details.stats.reduce((lhs, rhs) => lhs.base_stat > rhs.base_stat ? lhs : rhs).base_stat

  return (
    <View
      style={styles.summaryContainer}
    >
      <PokemonImage
        imageUrl={details.sprites.front_default}
      />
      <View
        style={styles.statContainer}
      >
        {details.stats.map((stat, idx) => (
          <View key={`${idx}`} style={styles.statRow}>
            <Text style={styles.statTitle}>{stat.stat.name}</Text>
            <View style={styles.statContent}>
              <View style={{position: 'absolute', backgroundColor: 'rgba(224, 224, 224, 1.0)', width: '100%', height: '100%'}} />
              <View style={{position: 'absolute', backgroundColor: 'rgba(244, 65, 54, 1.0)', width: statFactor(maxStat, stat.base_stat), height: '90%'}} />
              <Text style={{color: 'white', paddingLeft: 4}}>{stat.base_stat}</Text>
            </View>
          </View>
        ))}
      </View>
    </View>
  )
}

const statFactor = (maxStat: number, baseStat: number): string => `${(baseStat / maxStat) * 100}%`

const styles = StyleSheet.create({
  summaryContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'rgba(105, 240, 174, 1.0)',
  },
  pokemonImageContainer: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
  },
  statContainer: {
    flex: 0.7,
    paddingVertical: 8,
  },
  statRow: {
    flex: 0.5,
    paddingVertical: 6,
    flexDirection: 'row',
  },
  statTitle: {
    flex: 0.45,
  },
  statContent: {
    flex: 0.55,
    height: 20, 
    paddingRight: 20,
    justifyContent: 'center',
  },
})