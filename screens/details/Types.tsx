import React from 'react'
import styled from 'styled-components/native'
import { Type } from '../../model/model'
import { typeColor } from '../../model/pokemonTypeColor'

const TypesWrapper = styled.View`
  flex: 1;
  flexDirection: row;
`

interface TypeViewProps {
  type: Type
  typeCount: number
}

const TypeView = styled.View<TypeViewProps>`
  backgroundColor: ${props => typeColor(props.type)};
  width: ${props => 100 / props.typeCount}%;
  height: 50px;
  justifyContent: center;
`

const TypeName = styled.Text`
  fontSize: 18px;
  fontWeight: 500;
  color: white;
  paddingHorizontal: 18px;
`

export const Types = ({ types }: { types: Type[]}): JSX.Element => (
  <TypesWrapper>
    {types.map((type: Type, idx: number) => (
      <TypeView
        key={`${idx}`}
        typeCount={types.length}
        type={type}
      >
        <TypeName>{type.type.name}</TypeName>
      </TypeView>
    ))}
  </TypesWrapper>
)