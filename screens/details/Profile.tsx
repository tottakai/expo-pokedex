import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { GameIndices, PokemonDetails, PokemonSpecies } from '../../model/model'

const ProfileRow = ({title, content}: {title: string; content: string}): JSX.Element => (
  <View style={styles.profileRow}>
    <Text style={styles.rowTitle}>{title}</Text>
    <Text style={styles.rowContent}>{content}</Text>
  </View>
)

export const Profile = ({ details, species }: { details: PokemonDetails; species: PokemonSpecies | null }): JSX.Element => (
  species === null ? 
    <Text>Loading...</Text>
    :
    <View style={styles.profileContainer}>
      <Text style={styles.headerText}>Profile</Text>
      <View style={{ backgroundColor: 'white' }}>
        <ProfileRow title={'Weight:'} content={`${details.weight/10}kg`} />
        <ProfileRow title={'Height:'} content={`${details.height/10}m`} />
        <ProfileRow title={'Abilities:'} content={details.abilities.map((ability) => ability.ability.name).join(', ')} />
        <ProfileRow title={'Egg groups:'} content={species.egg_groups.map((eggGroup) => eggGroup.name).join(', ')} />
      </View>
      <Text style={styles.headerText}>{genus(species)}</Text>
      <Text style={styles.flavorText}>{flavor(details.game_indices, species)}</Text>
    </View>
)

const genus = (species: PokemonSpecies): string => 
  species.genera
    .filter((g) => g.language.name === 'en')
    .map((genus) => genus.genus)[0]

const flavor = (gameIndices: GameIndices[], species: PokemonSpecies): string => {
  if (gameIndices.length <= 0) {
    return ''
  }
  const [first] = gameIndices

  return species.flavor_text_entries
    .filter((entry) => 
      entry.language.name === 'en' &&
      entry.version.name === first.version.name)
    .map((entry) => entry.flavor_text)[0].replace(/[\f|\n]/g, ' ')
}


const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
    backgroundColor: 'rgba(83,155, 49, 1.0)',
  },
  headerText: { 
    fontSize: 22, 
    color: 'white', 
    padding: 20 
  },
  profileRow: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  rowTitle: {
    width: '40%',
    fontSize: 20,
    fontWeight: '700',
  },
  rowContent: {
    fontSize: 20,
  },
  flavorText: {
    padding: 20,
    backgroundColor: 'white'
  }
})