import { fold, right } from 'fp-ts/lib/Either'
import { pipe } from 'fp-ts/lib/function'
import wretch from 'wretch'
import { PokemonDetails, PokemonSpecies } from '../model/model'
import { GetPokemonDetailsResponse, GetPokemonListResponse, GetPokemonListSuccessResponse, GetPokemonSpeciesResponse } from './api-types'
import { onApiJsonValidationError } from './api-utils'

export const getPokemonList = async (offset: number): Promise<GetPokemonListResponse> => {
  console.log(`get pokemon list offset:${offset}`)
  return wretch(`https://pokeapi.co/api/v2/pokemon?limit=1000&offset=${offset}`)
    .get()
    .json((json) => pipe(
      GetPokemonListSuccessResponse.decode(json),
      fold(onApiJsonValidationError, (data) => right(data)))
    )
}

export const getPokemonDetails = async (url: string, signal: AbortSignal): Promise<GetPokemonDetailsResponse> => {
  console.log(`get Pokemon details for ${url}`)
  return wretch(url, {signal})
    .get()
    .json((json) => pipe(
      PokemonDetails.decode(json),
      fold(onApiJsonValidationError, (data) => right(data)
      )))
}

export const getPokemonSpecies = async (url: string, signal: AbortSignal): Promise<GetPokemonSpeciesResponse> => {
  console.log(`get Pokemon species for ${url}`)
  return wretch(url, {signal})
    .get()
    .json((json) => pipe(
      PokemonSpecies.decode(json),
      fold(onApiJsonValidationError, (data) => right(data)
      )))
}
