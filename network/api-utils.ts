import { Either, left } from 'fp-ts/lib/Either'
import * as t from 'io-ts'

export interface UnsupportedResponse {
  type: 'UNSUPPORTED_RESPONSE'
  messages: string[]
}

export const onApiJsonValidationError = (errors: t.Errors): Either<UnsupportedResponse, never> => {
  // eslint-disable-next-line no-console
  console.error('onApiJsonValidationError:', errors)
  return left<UnsupportedResponse>({
    type: 'UNSUPPORTED_RESPONSE',
    messages: errors.map((error) => error.context.map(({ key }) => key).join('.')),
  })
}