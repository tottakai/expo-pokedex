import { Either } from 'fp-ts/lib/Either'
import * as t from 'io-ts'
import { Pokemon, PokemonDetails, PokemonSpecies } from '../model/model'

export interface UnsupportedResponse {
  type: 'UNSUPPORTED_RESPONSE'
  messages: string[]
}

export type CommonApiError = UnsupportedResponse

export const GetPokemonListSuccessResponse = t.interface({
  results: t.array(Pokemon)
})
export type GetPokemonListSuccessResponse = t.TypeOf<typeof GetPokemonListSuccessResponse>
export type GetPokemonListResponse = Either<CommonApiError, GetPokemonListSuccessResponse>

export type GetPokemonDetailsResponse = Either<CommonApiError, PokemonDetails>

export type GetPokemonSpeciesResponse = Either <CommonApiError, PokemonSpecies>