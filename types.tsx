/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

import { ParamListBase } from '@react-navigation/native'
import { PokemonDetails } from './model/model'

export interface RootStackParamList extends ParamListBase {
  Home: undefined
  Details: { details: PokemonDetails; title: string }
  NotFound: undefined
}
