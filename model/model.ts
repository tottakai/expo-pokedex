import * as t from 'io-ts'

export const Pokemon = t.interface({
  name: t.string,
  url: t.string
})
export type Pokemon = t.TypeOf<typeof Pokemon>

export const Type = t.interface({
  type: t.interface({
    name: t.string,
  })
})
export type Type = t.TypeOf<typeof Type>

export const Species = t.interface({
  url: t.string
})

export const GameIndices = t.interface({
  version: t.interface({
    name: t.string,
  }),
})
export type GameIndices = t.TypeOf<typeof GameIndices>

export const PokemonDetails = t.interface({
  id: t.number,
  name: t.string,
  height: t.number,
  weight: t.number,
  sprites: t.interface({
    front_default: t.string,
  }),
  types: t.array(Type),
  species: Species,
  abilities: t.array(
    t.interface({
      ability: t.interface({
        name: t.string,
      }),
    }),
  ),
  game_indices: t.array(
    GameIndices
  ),
  stats: t.array(
    t.interface({
      base_stat: t.number,
      stat: t.interface({
        name: t.string,
      }),
    }),
  ),
})

export type PokemonDetails = t.TypeOf<typeof PokemonDetails>

export const PokemonSpecies = t.interface({
  flavor_text_entries: t.array(
    t.interface({
      language: t.interface({
        name: t.string
      }),
      version: t.interface({
        name: t.string
      }),
      flavor_text: t.string,
    }),
  ),
  genera: t.array(
    t.interface({
      genus: t.string,
      language: t.interface({
        name: t.string,
      }),
    }),
  ),
  egg_groups: t.array(
    t.interface({
      name: t.string,
    }),
  )
})

export type PokemonSpecies = t.TypeOf<typeof PokemonSpecies>
