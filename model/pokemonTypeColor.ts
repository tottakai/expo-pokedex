import { Type } from './model'

export const typeColor = (type: Type): string => {
  let color = '#ffffff'
  switch (type.type.name) {
  case 'grass':
    color = 'rgba(105, 194, 61, 1.0)'
    break
  case 'poison':
    color = 'rgba(146, 58, 146, 1.0)'
    break
  case 'fire':
    color = 'rgba(237, 109, 18, 1.0)'
    break
  case 'flying':
    color = 'rgba(142, 111, 235, 1.0)'
    break
  case 'water':
    color = 'rgba(69, 120, 237, 1.0)'
    break
  case 'bug':
    color = 'rgba(151, 165, 30, 1.0)'
    break
  case 'normal':
    color = 'rgba(156, 156, 99, 1.0)'
    break
  case 'electric':
    color = 'rgba(246, 201, 19, 1.0)'
    break
  case 'ground':
    color = 'rgba(219, 181, 77, 1.0)'
    break
  case 'fairy':
    color = 'rgba(232, 120, 144, 1.0)'
    break
  case 'fighting':
    color = 'rgba(174, 43, 36, 1.0)'
    break
  case 'psychic':
    color = 'rgba(247, 54, 112, 1.0)'
    break
  case 'rock':
    color = 'rgba(164, 143, 50, 1.0)'
    break
  case 'steel':
    color = 'rgba(160, 160, 192, 1.0)'
    break
  case 'ice':
    color = 'rgba(126, 206, 206, 1.0)'
    break
  case 'ghost':
    color = 'rgba(100, 78, 136, 1.0)'
    break
  case 'dragon':
    color = 'rgba(94, 29, 247, 1.0)'
    break
  case 'dark':
    color = 'rgba(100, 78, 64, 1.0)'
    break
  }
  return color
}